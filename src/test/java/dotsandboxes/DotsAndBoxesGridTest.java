package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Added an annotated @BeforeAll function
     */
    @BeforeAll
    public static void beforeAll() {
        logger.info("This message was printed out before any of the tests were run");
    }


    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }


    /*
     * textBoxComplete():
     * Purpose: Test used for testing whether the algorithm used for boxComplete() is wrong.
     */
    @Test
    public void testBoxComplete() {
        logger.info("Testing whether a box is complete");

        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(2, 2, 1);
        if (grid.drawHorizontal(0, 0, 0) &&
                grid.drawHorizontal(1, 0, 0) &&
                grid.drawVertical(0, 0, 0) &&
                grid.drawVertical(0, 1, 0)) {
            assertTrue(grid.boxComplete(0, 0));
        } else {
            assertFalse(grid.boxComplete(0, 0));
        }
    }


    /*
     * testIsSameHorizontalLine()
     * Purpose: Test used to throw an IllegalStateException if a line is drawn on an existing line.
     */
    @Test
    public void testIsSameHorizontalLine(){
        logger.info("Testing if a line has already been drawn");

        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(2,2, 1);
        grid.drawHorizontal(0,0,1);
        assertThrows(IllegalStateException.class, () -> grid.drawHorizontal(0,0,1),
                "Line already drawn");
    }

}
